<p align="center"><a href="#" target="_blank" rel="noopener noreferrer"><img width="100" src="logo.jpg" alt="LuatOS logo"></a></p>

[![license](https://img.shields.io/github/license/openLuat/LuatOS)](/LICENSE)

LuatOS系列开发板和扩展板

## 主开发板计划(main目录)

|开发板名称|核心功能|原理图进度|PCB进度|量产版本|备注|
|---------|-------|----------|------|-----------|----|
|luatos-air302| nbiot| Y | - | - | - |
|luatos-air640w| wifi| Y | - | - | - |
|luatos-w600| wifi| Y | - | - | - |
|luatos-w800| wifi+bt| Y | - | - | - |
|luatos-air724ug| cat.1| Y | - | - | - |
|luatos-esp32| wifi+bt| - | - | - | - |
|luatos-k210| ai+cam| - | - | - | - |
|luatos-air800h| 2G+GPS| - | - | - | - |
|luatos-air820h| 4G+GPS| - | - | - | - |
|luatos-stm32f1| mcu| - | - | - | - |
|luatos-stm32f4| mcu| - | - | - | - |
|luatos-air722ug| cat.1| - | - | - | - |

## 扩展板计划(exts目录)

|名称|目录|核心功能|供电|UART|GPIO|I2C|SPI|ADC/DAC|进度|备注|
|---------|----|---|----|----|---|---|---|-------|-------------|----|
|灯神 | 01-jinn| 点灯/按钮/继电器 | 5v+3.3V| N | Y | N | N | N | -- | - |
|数传 | 02-dtu  | 485/传感器      | 3.3V   | Y | Y | N | N | N | - | - |
|定位器|03-tracker| GPS          | 3.3V    | Y | Y | Y | N | N | - | -|
|显示|04-display| 显示屏         | 3.3V    | N | Y | Y | Y | N | - | -|
|拍它|05-p2p| nfc+spi flash    | 3.3V    | N | Y | N | Y | N | - | -|

## 第三方板子(other目录)

用于收集和展示各位大佬的工程杰作, 欢迎pull request或issue提供链接或文件

* [air640w-switch-kicad](other/air640w-switch-kicad) by 南阳-南风未起


## Luat 20pin硬件接口

适配Luat/LuatOS二次开发的硬件接口, 是本系列开发板的核心, 详细定义在doc目录.

## 授权协议

[MIT License](LICENSE)
