# luatos-module使用方法

#  1. 下载安装kicad

## 优点：

#### 1.kicad是一款开源软件，遵守GNU的GPL版权协议，使用起来没有盗版的问题。

#### 2.kicad采用图形操作界面，配合鼠标和键盘使用很方便。

#### 3.Kicad是一个跨平台的工具，在linux和windows都可以方便使用。

#### 4.Kicad生成gerber文件很简便，PCB画好以后可以直接生成gerber文件。不用担心文件格式

## 下载：

[清华大学镜像：kicad-5.1.10(win)](https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-5.1.10_1-x86_64.exe)

[清华大学镜像：kicad-5.99(win)](https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/nightly/kicad-msvc.r23295.1e21daf781-x86_64.exe)

[清华大学镜像：(macOS)](https://mirrors.tuna.tsinghua.edu.cn/kicad/osx/stable/)

## 安装：

双击exe文件，全部下一步，默认安装即可。

# 2.下载仓库

```
git clone --https://gitee.com/openLuat/luatos-broads.git
```

# 3.拷贝luatos-module

此目的为将luatos-module的元件封装加入自己电脑的kicad库中。

![路径](https://yajs666.gitee.io/images/114531.png)

如图，将仓库luatos-module文件夹全部拷贝至kicad的library文件夹即可

## 4.配置路径

打开桌面的kicad软件

<!-- ![配置](C:\Users\Administrator\Desktop\屏幕截图 2021-07-16 115653.png) -->
![配置](https://yajs666.gitee.io/images/115653.png)

点击首选项，分别进入管理符号库和封装库两个选型

![符号库](https://yajs666.gitee.io/images/115956.png)

![封装库](https://yajs666.gitee.io/images/120146.png)

# 5.打开工程

![main](https://yajs666.gitee.io/images/120358.png)

打开自己luatos-broads文件夹任意工程的.pro文件即可打开kicad软件

![原理图&PCB](https://yajs666.gitee.io/images/121038.png)

![3D](https://yajs666.gitee.io/images/121229.png)

pcb工程下快捷键alt+3打开工程3D图，缺少3d封装的元件还需要手动添加